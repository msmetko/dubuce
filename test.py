import torch

def f(x, a, b):
    return a * x + b

a = torch.tensor(5.0, requires_grad=True)
b = torch.tensor(8.0, requires_grad=True)
x = torch.tensor(2.0)
y = f(x, a, b)
s = a ** 2

y.backward()
s.backward()
assert x.grad is None
assert a.grad == x + 2*a
assert b.grad == 1

print(f"y={y}, g_a={a.grad}, g_b={b.grad}")
