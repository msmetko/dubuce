#!/usr/bin/env bash
# ARG PARSING
video_name=out.avi
resize='1000%'
temp_dir='/tmp/temp'
framerate='10';
POSITIONAL=();
if [[ $# == 0 ]]; then
    echo "There are no image files. Aborting."
    exit 1;
fi
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -o|--output)
            video_name="$2"
            shift; shift;
            ;;
        -r|--resize)
            resize=$2;
            shift; shift;
            ;;
        -f|--framerate)
            framerate=$2;
            shift; shift;
            ;;
        -h|--help)
            echo "make_video.sh [-o output_file -r resize_factor -f framerate] (one or more images)";
            exit;
            ;;
        *)
            POSITIONAL+=("$1")
            shift;
            ;;
    esac
done

echo "Resizing ${#POSITIONAL[@]} images to ${resize} and converting it to ${video_name}";

mkdir $temp_dir
mogrify -interpolate Nearest -filter point -resize 1000% -quality 100 -path ${temp_dir} ${POSITIONAL[@]}
ffmpeg -nostats -loglevel 0 -framerate $framerate -i "${temp_dir}/%*.png" ${video_name}
rm -rf $temp_dir
echo "Done."
