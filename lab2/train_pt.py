import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor
import skimage as ski
import skimage.io as io
import math
from pathlib import Path
import os

DATA_DIR = Path(__file__).parent / 'MNIST'
SAVE_DIR = Path(__file__).parent / 'pt'

class ConvolutionalModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 16, kernel_size=5, padding=2)
        self.pool1 = nn.MaxPool2d(2)
        #self.relu1 = nn.ReLU()
        self.conv2 = nn.Conv2d(16,32, kernel_size=5, padding=2)
        self.pool2 = nn.MaxPool2d(2)
        #self.relu2 = nn.ReLU()
        # self.flatten = nn.Flatten()
        self.fc1 = nn.Linear(1568, 512, bias=True)
        #self.relu3 = nn.ReLU()
        self.fc2 = nn.Linear(512, 10, bias=True)
        self.reset_parameters()
        return

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear) and m is not self.fc2:
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc2.reset_parameters()
        return

    def forward(self, x):
        x = self.conv1(x)
        x = self.pool1(x)
        x = torch.relu(x) # x = self.relu1(x)
        x = self.conv2(x)
        x = self.pool2(x)
        x = torch.relu(x) # x = self.relu2(x)
        x = x.view(x.shape[0], -1) # x = self.flatten(x)
        x = self.fc1(x)
        x = torch.relu(x) # x = self.relu3(x)
        x = self.fc2(x)
        return x

def draw_conv_filters(epoch, step, weights, save_dir):
     w = weights.copy()
     num_filters = w.shape[0]
     num_channels = w.shape[1]
     k = w.shape[2]
     assert w.shape[3] == w.shape[2]
     w = w.transpose(2,3,1,0)
     w -= w.min()
     w /= w.max()
     border = 1
     cols = 8
     rows = math.ceil(num_filters / cols)
     width = cols * k + (cols-1) * border
     height = rows * k + (rows-1) * border
     img = np.zeros([height, width, num_channels])
     for i in range(num_filters):
         r = int(i / cols) * (k + border)
         c = int(i % cols) * (k + border)
         img[r:r+k,c:c+k,:] = w[:,:,:,i]
     filename = 'epoch_%02d_step_%06d.png' % (epoch, step)
     ski.io.imsave(os.path.join(save_dir, filename), img)
     return

if __name__ == "__main__":
    ds_train = MNIST(DATA_DIR, train=True, download=True, transform=ToTensor())
    ds_test = MNIST(DATA_DIR, train=False, transform=ToTensor())
    EPOCH_COUNT = 8
    BATCH_SIZE = 50
    WEIGHT_DECAY = 1e-1 #1e-4
    SAVE_DIR = SAVE_DIR / ('out-'+str(WEIGHT_DECAY))
    LR = 1e-1
    
    ds_train.data = ds_train.data.float().reshape([-1, 1, 28, 28]) / 255.0
    train_mean = ds_train.data.mean()
    ds_train.data -= train_mean
    ds_test.data = ds_test.data.float().reshape([-1, 1, 28, 28]) / 255.0 - train_mean
    cutoff = 55000
    train_loader = data.DataLoader(data.TensorDataset(ds_train.data[:cutoff], ds_train.targets[:cutoff]), batch_size=BATCH_SIZE, shuffle=True)
    val_loader = data.DataLoader(data.TensorDataset(ds_train.data[cutoff:], ds_train.targets[cutoff:]), batch_size=5000, shuffle=False)
    test_loader = data.DataLoader(data.TensorDataset(ds_test.data, ds_test.targets), batch_size=ds_test.data.shape[0])
    model = ConvolutionalModel()
    loss = nn.CrossEntropyLoss()
    optimizer = optim.SGD([
        {"params": [
            *model.conv1.parameters(),
            *model.conv2.parameters(),
            *model.fc1.parameters()], "weight_decay": WEIGHT_DECAY},
        {"params": [*model.fc2.parameters()], "weight_decay": 0.0}
        ], lr=LR)
    # this lr sch works well
    scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[2, 4, 6], gamma=0.1)
    for epoch in range(EPOCH_COUNT):
        running_loss = 0.0
        running_correct = 0.0
        print('lr =', next(t['lr'] for t in optimizer.state_dict()['param_groups'] if 'lr' in t))
        print('epoch \t batch \t loss')
        for i, data in enumerate(train_loader, 0):
            inputs, labels = data
            optimizer.zero_grad()
            outputs = model(inputs)
            batch_loss = loss(outputs, labels)
            batch_loss.backward()
            optimizer.step()
            running_loss += batch_loss.item()
            running_correct += torch.mean((labels == torch.argmax(outputs, dim=1)).float()).item()
            if i % 5 == 0:
                print(f"{epoch} \t {i} \t {batch_loss.item()}")
            if i % 100 == 0:
                draw_conv_filters(epoch, i, model.conv1.weight.detach().numpy(), SAVE_DIR)
        print(f"{epoch} \t total \t {running_loss / len(train_loader)}")
        print(f"{epoch} train acc \t {running_correct / len(train_loader) * 100}")
        # validate
        for data in val_loader:
            inputs, labels = data
            outputs = model(inputs)
            val_loss = loss(outputs, labels)
            val_acc = torch.mean((labels == torch.argmax(outputs, dim=1)).float())
        print(f"{epoch} \t val \t {val_loss.item()}")
        print(f"{epoch} \t val acc \t {val_acc.item()}")
        scheduler.step()
    for inputs, labels in test_loader:
        outputs = model(inputs)
        test_loss = loss(outputs, labels)
        test_acc = torch.mean((labels == torch.argmax(outputs, dim=1)).float())
    print(f"-1 \t test loss \t {test_loss}")
    print(f"-1 \t test acc \t {test_acc}")
