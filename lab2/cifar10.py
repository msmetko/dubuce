import itertools as it
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import skimage as ski
import skimage.io as io
from torchvision.datasets import CIFAR10
from torchvision.transforms import ToTensor
from pathlib import Path
import os
import math

class ConvolutionalModel(nn.Module):
    def __init__(self, n_classes):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 16, kernel_size=5, stride=1, padding=0)
        self.pool1 = nn.MaxPool2d(3, 2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=0)
        self.pool2 = nn.MaxPool2d(3, 2)
        self.flatten = nn.Flatten()
        self.fc1 = nn.Linear(512, 256)
        self.fc2 = nn.Linear(256, 128)
        self.relu4 = nn.ReLU()
        self.fc3 = nn.Linear(128, n_classes)
        #self.reset_parameters()
        return

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                k = np.sqrt(1 / (m.in_channels * m.kernel_size[0] * m.kernel_size[1]))
                #nn.init.normal_(m.weight, 0, k)
                nn.init.kaiming_normal_(m.weight, mode='fan_in')
                nn.init.zeros_(m.bias)
            elif isinstance(m, nn.Linear) and m is not self.fc3:
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                nn.init.zeros_(m.bias)
        self.fc3.reset_parameters()
        return

    def forward(self, X):
        X = self.conv1(X)
        X = torch.relu(X)
        X = self.pool1(X)
        X = self.conv2(X)
        X = torch.relu(X)
        X = self.pool2(X)
        X = self.flatten(X)
        X = self.fc1(X)
        X = torch.relu(X)
        X = self.fc2(X)
        X = torch.relu(X)
        X = self.fc3(X)
        return X

def draw_conv_filters(epoch, step, weights, save_dir):
    w = weights.copy()
    num_filters = w.shape[0]
    num_channels = w.shape[1]
    k = w.shape[2]
    assert w.shape[3] == w.shape[2]
    w = w.transpose(2,3,1,0)
    w -= w.min()
    w /= w.max()
    border = 1
    cols = 8
    rows = math.ceil(num_filters / cols)
    width = cols * k + (cols-1) * border
    height = rows * k + (rows-1) * border
    img = np.zeros([height, width, num_channels])
    for i in range(num_filters):
        r = int(i / cols) * (k + border)
        c = int(i % cols) * (k + border)
        img[r:r+k,c:c+k,:] = w[:,:,:,i]
    img = (img - np.min(img)) / (np.max(img) - np.min(img))
    img = ski.util.img_as_ubyte(img)
    filename = 'epoch_%02d_step_%06d.png' % (epoch, step)
    ski.io.imsave(os.path.join(save_dir, filename), img)
    return

def preprocess(train_ds, test_ds, valid_size=5000, device=None):
    train_val = torch.tensor(train_ds.data, dtype=torch.double)
    assert train_val.shape == (50000, 32, 32, 3)
    val_x = train_val[:valid_size, :, :, :]
    train_x = train_val[valid_size:, :, :, :]
    train_mean = train_x.mean(dim=[0, 1, 2])
    # print(train_mean) # tensor([125.3108,122.9714, 113.9238])
    train_std = train_x.std(dim=[0, 1, 2])
    # print(train_std) # tensor([62.9771, 62.0599, 66.6718])
    train_x = (train_x - train_mean) / train_std
    # print("OK so far")
    val_x = (val_x - train_mean) / train_std
    test_x = (torch.tensor(test_ds.data, dtype=torch.double) - train_mean) / train_std
    train_x = train_x.permute([0, 3, 1, 2])
    val_x = val_x.permute([0, 3, 1, 2])
    test_x = test_x.permute([0, 3, 1, 2])
    return (train_x.float().to(device), torch.tensor(train_ds.targets[valid_size:], dtype=torch.long).to(device),
            val_x.float().to(device), torch.tensor(train_ds.targets[:valid_size], dtype=torch.long).to(device),
            test_x.float().to(device), torch.tensor(test_ds.targets, dtype=torch.long).to(device),
            train_mean, train_std)

def accuracy(cm):
    # return cm[0, 0] / np.sum(np.diag(cm))
    return np.sum(np.diag(cm)) / np.sum(cm)

def recall(cm):
    return cm[0, 0] / np.sum(cm[:, 0])

def precision(cm):
    return cm[0, 0] / np.sum(cm[0, :])

def f1score(precision, recall):
    return 2*(precision * recall) / (precision + recall)

def get_class_cm(cm, index):
    """
    This function takes a confusion matrix from sklearn
    and returns a perclass binary confusion matrix
    
    Note that this function changes conventions!
    - sklearn's CM has true labels at rows and predicted at columns
        [[tp, fn],
         [fp, tn]]
    - this CM has true labels at columns and predicted at rows
    this means this CM is arranged as:
        [[tp, fp],
         [fn, tn]]
    """
    tp = cm[index, index]
    fn = np.sum(cm[index, :]) - tp
    fp = np.sum(cm[:, index]) - tp
    tn = np.sum(cm) - tp - fp - fn
    return np.array([[tp, fp],[fn, tn]])

def metrics(y_pred, y_true, metric_list):
    metric_dict = dict()
    N = y_pred.shape[0]
    assert y_pred.shape == (N,), f"y_pred.shape == {y_pred.shape}"
    assert y_true.shape == (N,), f"y_true.shape == {y_true.shape}"
    cm = confusion_matrix(y_true, y_pred)
    real_accuracy = accuracy(cm)
    #print(cm)
    class_cms = [get_class_cm(cm, i) for i in range(len(cm))]
    accs = []
    prcs = []
    recs = []
    f1s = []
    print("class\tacc\tprc\trec\tf1")
    for i, class_cm in enumerate(class_cms):
        #print(class_cm)
        #raise Exception
        acc = accuracy(class_cm)
        prc = precision(class_cm)
        rec = recall(class_cm)
        f1 = f1score(prc, rec)
        print(f'{i}\t{acc:.03}\t{prc:.03}\t{rec:.03}\t{f1:.03}')
        accs.append(acc)
        prcs.append(prc)
        recs.append(rec)
        f1s.append(f1)
    metric_dict['macro-accuracy'] = real_accuracy #np.mean(accs)
    metric_dict['macro-precision'] = np.mean(prcs)
    metric_dict['macro-recall'] = np.mean(recs)
    metric_dict['macro-f1'] = np.mean(f1s)
    print(f"macro\t{metric_dict['macro-accuracy']:.03}\t{metric_dict['macro-precision']:.03}\t{metric_dict['macro-recall']:.03}\t{metric_dict['macro-f1']:.03}")
    micro_cm = sum(class_cms)
    metric_dict['micro-accuracy'] = real_accuracy    #accuracy(micro_cm)
    metric_dict['micro-precision'] = precision(micro_cm)
    metric_dict['micro-recall'] = recall(micro_cm)
    metric_dict['micro-f1'] = f1score(metric_dict['micro-precision'], metric_dict['micro-recall'])
    print(f"micro\t{metric_dict['micro-accuracy']:.03}\t{metric_dict['micro-precision']:.03}\t{metric_dict['micro-recall']:.03}\t{metric_dict['micro-f1']:.03}")
    return {t: metric_dict[t] for t in metric_list} if metric_list else None

def evaluate(phase, model, dataloader, loss, metric_list=None):
    print(phase + ":")
    label_list = []
    output_list = []
    loss_list = []
    for inputs, labels in dataloader:
        label_list.append(labels.cpu().numpy())
        outputs = model(inputs)
        output_list.append(torch.argmax(outputs, dim=1).cpu().numpy())
        batch_loss = loss(outputs, labels)
        loss_list.append(batch_loss.item())
    avg_loss = np.mean(loss_list)
    print("loss:", avg_loss)
    return_metrics = metrics(np.hstack(output_list), np.hstack(label_list), metric_list)
    return_metrics['loss'] = avg_loss
    return return_metrics


def plot_training_progress(plot_data, directory, metric_list, plot_name='metric_plot.png'):
    fig, axes = plt.subplots(2, 5, figsize=(15, 3))
    axes = axes.ravel()
    axes[0].plot(plot_data['train']['loss'], label='train')
    axes[0].plot(plot_data['val']['loss'], label='val')
    axes[0].legend()
    axes[0].set_title("Loss")
    axes[5].plot(plot_data['lr'], label='learning rate')
    axes[5].legend()
    axes[5].set_title("Learning rate")
    for i, metric_name in enumerate(sorted(metric_list)):
        if 'macro' in metric_name:
            index = 1 + i
        else:
            index = 2 + i
        axes[index].plot(plot_data['train'][metric_name], label='Train')
        axes[index].plot(plot_data['val'][metric_name], label='Val')
        axes[index].legend()
        axes[index].set_title(metric_name)
    save_path = os.path.join(directory, plot_name)
    print(f"Saving data in {save_path}")
    plt.savefig(save_path)
    return

def train(model, train_loader, val_loader, test_loader, loss, optimizer, lr_scheduler=None, epochs=8, metric_list=[], save=True):
    plot_data = {
        "train": {metric: [] for metric in metric_list},
        "val": {metric: [] for metric in metric_list},
        "lr": []}
    plot_data['train']['loss'] = []
    plot_data['val']['loss'] = []
    for epoch in range(epochs):
        print("epoch\tbatch\ttype\tvalue")
        for i, (inputs, labels) in enumerate(train_loader):
            outputs = model(inputs)
            batch_loss = loss(outputs, labels)
            if i % 10 == 0:
                print(f"{epoch}\t{i}\tloss\t{batch_loss.item()}")
            if i % 50 == 0:
                draw_conv_filters(epoch, i, model.conv1.weight.detach().cpu().numpy(), SAVE_DIR)
                pass
            batch_loss.backward()
            optimizer.step()
            optimizer.zero_grad()
        with torch.no_grad():
            for phase, loader in zip(["train", "val"], [train_loader, val_loader]):
                metrics = evaluate(phase, model, loader, loss, metric_list)
                for metric_name, value in metrics.items():
                    plot_data[phase][metric_name] += [value]
        plot_data['lr'] += [lr_scheduler.get_last_lr()]
        if lr_scheduler:
            lr_scheduler.step()
    metrics = evaluate("test", model, test_loader, loss, metric_list)
    if save:
        torch.save(model.state_dict(), SAVE_DIR / ('model_'+str(metrics['macro-accuracy'])+'.pt'))
    return plot_data

def plot_20(images, mean, std, labels, classes, class_indices, confidences, save_dir):
    plt.figure(figsize=(10, 13))
    plt.tight_layout(h_pad=5)
    for i in range(20):
        plt.subplot(4, 5, i+1)
        img = images[i]
        img *= std
        img += mean
        plt.imshow(img.astype(np.uint8).transpose((1, 2, 0)))
        plt.title(classes[labels[i]])
        plt.xlabel("\n".join([f"{classes[class_indices[i, j].item()]} {confidences[i, j]:.04}" for j in range(len(class_indices[i]))]))
    plt.savefig(save_dir / ('top_20.png'))
    return


if __name__ == "__main__":
    DATA_DIR = Path(__file__).parent / "CIFAR10"
    SAVE_DIR = Path(__file__).parent / "cifar_data"
    BATCH_SIZE = 50
    EPOCHS = 50 #50
    metric_list = ['-'.join(t) for t in it.product(['micro', 'macro'], ['accuracy', 'precision', 'recall', 'f1'])]
    device = "cuda" if torch.cuda.is_available() else "cpu"
    train_ds = CIFAR10(DATA_DIR, train=True, download=True, transform=ToTensor())
    test_ds = CIFAR10(DATA_DIR, train=False, download=True, transform=ToTensor())
    assert train_ds.data.shape == (50000, 32, 32, 3), f"Shape is {train_ds.data.shape}"
    X_train, y_train, X_val, y_val, X_test, y_test, mean, std = preprocess(train_ds, test_ds, device=device)
    train_loader = data.DataLoader(data.TensorDataset(X_train, y_train),
            batch_size=BATCH_SIZE, shuffle=True)
    val_loader = data.DataLoader(data.TensorDataset(X_val, y_val),
            batch_size=X_val.shape[0])
    test_loader = data.DataLoader(data.TensorDataset(X_test, y_test),
            batch_size=X_test.shape[0])
    analysis_loader = data.DataLoader(data.TensorDataset(X_test, y_test), batch_size=1)
    
    model = ConvolutionalModel(10).float().to(device)
    loss = nn.CrossEntropyLoss()
    optimizer = optim.SGD([
        {'params': [t for t in model.parameters() if t is not model.fc3.weight and t is not model.fc3.bias],
         'weight_decay': 0.013},
        {'params': model.fc3.parameters(), 'weight_decay': 0.0025}
    ],lr=0.025)
    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.950)
    plot_data = train(model,
        train_loader,
        val_loader,
        test_loader,
        loss, optimizer, scheduler,
        epochs=EPOCHS, metric_list=metric_list,
        save=True)
    with torch.no_grad():
        losses_list = []
        outputs_list = []
        labels_list = []
        for inputs, labels in analysis_loader:
            outputs = model(inputs)
            image_loss = loss(outputs, labels)
            losses_list.append(image_loss.item())
            outputs_list.append(outputs)
            labels_list.append(labels)
        losses = torch.tensor(losses_list)
        indices = torch.argsort(losses, descending=True)[:20]
        outputs = nn.functional.softmax(torch.cat(outputs_list)[indices, :], dim=1).detach()
        labels = torch.cat(labels_list)[indices]
        class_indices = torch.argsort(outputs, dim=1, descending=True)[:, :3]
        predictions = torch.stack([outputs[i, class_indices[i, :]] for i in range(outputs.shape[0])])
        plot_20(X_test[indices].detach().cpu().numpy(),
                mean.detach().cpu().numpy().reshape(-1, 1, 1),
                std.detach().cpu().numpy().reshape(-1, 1, 1),
                labels.detach().cpu().numpy().ravel(),
                train_ds.classes, 
                class_indices.detach().cpu().numpy(),
                predictions.detach().cpu().numpy(),
                SAVE_DIR)
