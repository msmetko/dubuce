import time
from pathlib import Path

import numpy as np
from torchvision.datasets import MNIST

import nn
import layers

# DATA_DIR = Path(__file__).parent / 'datasets' / 'MNIST'
DATA_DIR = Path(__file__).parent / 'MNIST'
SAVE_DIR = Path(__file__).parent / 'out'

config = {}
config['max_epochs'] = 8
config['batch_size'] = 50
config['save_dir'] = SAVE_DIR
config['lr_policy'] = {1:{'lr':1e-1}, 3:{'lr':1e-2}, 5:{'lr':1e-3}, 7:{'lr':1e-4}}

def dense_to_one_hot(y, class_count):
    return np.eye(class_count)[y]

#np.random.seed(100) 
np.random.seed(int(time.time() * 1e6) % 2**31)

ds_train, ds_test = MNIST(DATA_DIR, train=True, download=True), MNIST(DATA_DIR, train=False)
train_x = ds_train.data.reshape([-1, 1, 28, 28]).numpy().astype(np.float) / 255
train_y = ds_train.targets.numpy()
train_x, valid_x = train_x[:55000], train_x[55000:]
train_y, valid_y = train_y[:55000], train_y[55000:]
test_x = ds_test.data.reshape([-1, 1, 28, 28]).numpy().astype(np.float) / 255
test_y = ds_test.targets.numpy()
train_mean = train_x.mean()
train_x, valid_x, test_x = (x - train_mean for x in (train_x, valid_x, test_x))
train_y, valid_y, test_y = (dense_to_one_hot(y, 10) for y in (train_y, valid_y, test_y))


net = []
inputs = np.random.randn(config['batch_size'], 1, 28, 28)#input         param           output          rp
net += [layers.Convolution(inputs, 16, 5, "conv1")]     # (Bx1x28x28)   16*1*5*5*16     (Bx16x28x28)    5x5
net += [layers.MaxPooling(net[-1], "pool1")]            # (Bx16x28x28)  0               (Bx16x14x14)    6x6
net += [layers.ReLU(net[-1], "relu1")]                  # (Bx16x14x14)  0               (Bx16x14x14)    6x6
net += [layers.Convolution(net[-1], 32, 5, "conv2")]    # (Bx16x14x14)  32*16*5*5+32    (Bx32x14x14)    14x14
net += [layers.MaxPooling(net[-1], "pool2")]            # (Bx32x14x14)  0               (Bx32x7x7)      16x16
net += [layers.ReLU(net[-1], "relu2")]                  # (Bx32x7x7)    0               (Bx32x7x7)      16x16
# out = 7x7
net += [layers.Flatten(net[-1], "flatten3")]            # (Bx32x7x7)    0               (Bx1568)        16x16  
net += [layers.FC(net[-1], 512, "fc3")]                 # (Bx1568)      1568*512+512    (Bx512)         28x28
net += [layers.ReLU(net[-1], "relu3")]                  # (Bx512)       0               (Bx512)         28x28
net += [layers.FC(net[-1], 10, "logits")]               # (Bx512)       512*10 + 10     (Bx10)          28x28
# total params: 827690
# total memory: 1580500B = 1.5 MB
loss = layers.SoftmaxCrossEntropyWithLogits()
print(train_x.dtype)
raise Exception
nn.train(train_x, train_y, valid_x, valid_y, net, loss, config)
nn.evaluate("Test", test_x, test_y, net, loss, config)
# 16*1*5*5*16+32*16*5*5+32+1568*512+512+0+512*10+10 
# RESULTS:
# Train acc = 99.5
# Val acc = 99.10
# val avg los = 0.03
# test acc = 99.10
# test avg loss = 0.03

