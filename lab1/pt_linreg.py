import torch
import torch.nn as nn
import torch.optim as optim


## Definicija računskog grafa
# podaci i parametri, inicijalizacija parametara
a = torch.randn(1, requires_grad=True)
b = torch.randn(1, requires_grad=True)

X = torch.tensor([1.0, 2.0, 3.0, 4.0])
Y = torch.tensor([3, 5, 6.8, 9.2])

# optimizacijski postupak: gradijentni spust
optimizer = optim.SGD([a, b], lr=0.05)

for i in range(250):
    # afin regresijski model
    Y_ = a*X + b

    diff = (Y-Y_)

    # kvadratni gubitak
    loss = torch.mean(diff**2)

    # računanje gradijenata
    loss.backward()

    grad_a = -torch.dot(2 * diff, X) / X.shape[0]
    grad_b = (-2 / X.shape[0]) * torch.sum(diff)
    assert torch.abs(a.grad - grad_a) < 1e-10
    assert torch.abs(b.grad - grad_b) < 1e-10

    print(f'({i})\n\tloss: {loss}\n\tY_:{Y_.data}\n\ta:{a.data}\t grad: {a.grad}\n\tb: {b.data}\tgrad: {b.grad}\n')

    # korak optimizacije
    optimizer.step()

    # Postavljanje gradijenata na nulu
    optimizer.zero_grad()

