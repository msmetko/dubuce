import torch
import torchvision
import numpy as np
from sklearn.multiclass import OneVsOneClassifier
from sklearn.svm import SVC

dataset_root = '/tmp/mnist'  # change this to your preference
mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=True)
mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=True)

X_train, y_train = mnist_train.data, mnist_train.targets
X_test, y_test = mnist_test.data, mnist_test.targets
X_train, X_test = X_train.float().div_(255.0).numpy().reshape((-1, 28*28)), X_test.float().div_(255.0).numpy().reshape((-1, 28*28))
y_train, y_test = y_train.numpy(), y_test.numpy()
ovo = OneVsOneClassifier(SVC(C=1.0, kernel='linear', gamma='scale'), n_jobs=-1)
ovo.fit(X_train, y_train)
y_pred = ovo.predict(X_test)
print(f"Accuracy: {np.mean(y_pred == y_test)}")
#print(X_train.shape, y_train.shape, X_test.shape, y_test.shape, y_pred.shape)
