import numpy as np
import matplotlib.pyplot as plt

def onehotencode(y):
    """
    https://stackoverflow.com/questions/29831489
    """
    a = y.flatten()
    b = np.zeros((a.size, a.max()+1), dtype=np.float64)
    b[np.arange(a.size), a] = 1.0
    return b

class Random2DGaussian:
    def __init__(self, mu=None, sigma=None, x_min=0, x_max=10, y_min=0, y_max=10):
      self.x_min = x_min
      self.x_max = x_max
      self.y_min = y_min
      self.y_max = y_max
      self.mu = mu if mu is not None else np.array([x_min + (x_max - x_min) * np.random.random(),
                                                    y_min + (y_max - y_min) * np.random.random()])
      if sigma is None:
          D = np.array([[(np.random.sample()*((x_max - x_min)/5))**2, 0],
                        [0, (np.random.sample()*((y_max - y_min)/5))**2]])
          theta = np.random.sample() * (2 * np.pi)
          c, s = np.cos(theta), np.sin(theta)
          R = np.array([[c, -s], [s, c]])
          self.sigma = R.T @ D @ R
      else:
          self.sigma = sigma
      return

    def get_sample(self, n):
      X = np.random.randn(n, 2)
      return X @ self.sigma + self.mu

def sample_gauss_2d(C, N):
    X = []
    y = []
    for i in range(C):
      G = Random2DGaussian()
      X.append(G.get_sample(N))
      y.append(np.full((N, 1), i))
    return np.vstack(X), np.vstack(y)

def eval_perf_binary(y_pred, y_true):
    TP = np.sum(np.logical_and(y_true == True, y_pred == y_true))
    TN = np.sum(np.logical_and(y_true == False, y_pred == y_true))
    FP = np.sum(np.logical_and(y_true == False, y_pred != y_true))
    FN = np.sum(np.logical_and(y_true == True, y_pred != y_true))
    accuracy = (TP + TN) / (TP + FP + TN + FN)
    precision = TP / (TP + FP)
    recall = TP / (TP + FN)
    return accuracy, precision, recall


def eval_AP(ranked):
    precisions = []
    N = ranked.shape[0]
    for k in range(N):
      if ranked[k] == 1:
          precisions.append(np.sum(ranked[k:]) / (N-k))
    return sum(precisions) / len(precisions) if len(precisions) > 0 else 0.0

def graph_data(X, y_true, y_pred, special=[]):
    y_true = y_true.flatten()
    y_pred = y_pred.flatten()
    plt.scatter(X[y_true == y_pred, 0],
              X[y_true == y_pred, 1],
              #c=y_true[y_true == y_pred],
              marker="o", s=10, c='green', label='Correct')
    plt.scatter(X[y_true != y_pred, 0],
              X[y_true != y_pred, 1],
              #c=y_true[y_true != y_pred],
              marker="s", s=10, c='red', label='Incorrect')
    if special != []:
        plt.scatter(X[special, 0], X[special, 1], s=20, c='yellow', label='Support')
    plt.legend()
    return

def myDummyDecision(X):
    return -12*X[:, 0] + 42*X[:, 1] - 30

def graph_surface(function, rect, offset=0.5, width=256, height=256):
    """Creates a surface plot (visualize with plt.show)

    Arguments:
    function: surface to be plotted
    rect:     function domain provided as:
              ([x_min,y_min], [x_max,y_max])
    offset:   the level plotted as a contour plot
    Returns:
    None
      """

    lsw = np.linspace(rect[0][1], rect[1][1], width) 
    lsh = np.linspace(rect[0][0], rect[1][0], height)
    xx0,xx1 = np.meshgrid(lsh, lsw)
    grid = np.stack((xx0.flatten(),xx1.flatten()), axis=1)

    #get the values and reshape them
    values=function(grid).reshape((width,height))
    
    # fix the range and offset
    delta = offset if offset else 0
    maxval=max(np.max(values)-delta, - (np.min(values)-delta))
    
    # draw the surface and the offset
    plt.pcolormesh(xx0, xx1, values, 
       vmin=delta-maxval, vmax=delta+maxval, cmap='gist_rainbow')
      
    if offset != None:
        plt.contour(xx0, xx1, values, colors='black', levels=[offset])
    return

def sample_gmm_2d(K, C, N):
    """
    K   the total number of distributions
    C   the number of classes
    N   the number of generated examples in every distribution
    """

    X = []
    y = []
    for i in range(K):
        # assure every class is selected at least once
        C_i = i if i < C else np.random.choice(C) # i-th class number
        G = Random2DGaussian()
        X.append(G.get_sample(N))
        y.append(np.full((N, 1), C_i))

    return np.vstack(X), np.vstack(y)

if __name__ == "__main__":
    assert eval_AP(np.array([0, 0, 0, 1, 1, 1])) == 1.0
    assert eval_AP(np.array([0, 0, 1, 0, 1, 1])) == 11/12
    assert eval_AP(np.array([0, 1, 0, 1, 0, 1])) == 34/45
    assert eval_AP(np.array([1, 0, 1, 0, 1, 0])) == 0.5
    #X, Y = sample_gauss_2d(2, 100)
    X, Y = sample_gmm_2d(4, 2, 30)
    y = (myDummyDecision(X)>= 0.5)
    #print(Y.shape, y.shape)
    #plt.show()
    rect = (np.min(X, axis=0), np.max(X, axis=0))
    graph_surface(myDummyDecision, rect, 0.0, 640, 480)
    graph_data(X, Y, y)
    plt.show()
