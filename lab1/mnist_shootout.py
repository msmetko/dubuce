import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import data
import matplotlib.pyplot as plt
import numpy as np

class PTDeep(nn.Module):
    def __init__(self, layer_list, activation=F.relu):
        super().__init__()
        self.weights = nn.ParameterList()
        self.biases = nn.ParameterList()
        for i, o in zip(layer_list, layer_list[1:]):
            self.weights.append(nn.Parameter(torch.randn(i, o)*0.01))
            self.biases.append(nn.Parameter(torch.zeros(o)))
        self.activation = activation
        return

    def forward(self, X):
        """ X should be a torch.Tensor"""
        for w, b in zip(self.weights, self.biases):
            X = self.activation(torch.mm(X, w) + b)
        return F.softmax(X, dim=1)

    def get_loss(self, Y, Yoh, param_lambda):
        s = 0
        if param_lambda > 0:
            s = param_lambda * sum(torch.sum(w.T @ w) for w in self.weights)
        X = torch.sum(torch.log(Y) * Yoh, 1)
        return -torch.mean(X) + s

    def count_params(self):
        param_sum = 0
        text = "Name\t\tCount\n"
        for name, param in self.named_parameters():
            param_count = np.prod(param.shape)
            text += f"{name}\t{param_count}\n"
            param_sum += param_count
        text += f"\nTotal\t\t{param_sum}"
        return text

def train(model, X, Yoh, param_niter=1000, param_delta=0.1, param_lambda=1.0, device='cpu', validate=False, metric=None):
    min_metric = None 
    min_model = None
    if validate:
        percent = 0.2
        # exact proportion
        N = X.shape[0]
        proportion = int(N * percent)
        val_mask = np.array([1] * (proportion) + [0] * (N - proportion))
        np.random.shuffle(val_mask)
        X_val = X[val_mask]
        Yoh_val = Yoh[val_mask]
        X = X[1 - val_mask]
        Yoh = Yoh[1-val_mask]

    optimizer = optim.SGD(model.parameters(), lr=param_delta)
    for i in range(param_niter):
        Y = model(X)
        loss = model.get_loss(Y, Yoh, param_lambda)
        loss.backward()
        if i % 20 == 0:
            print(f"({i})\t{loss}")
        if validate:
            Y_pred = model(X_val)
            if metric:
                result = metric(Y_pred, Yoh_val)
            else:
                result = model.get_loss(Y_pred, Yoh_val, param_lambda)

            if i % 20 == 0:
                 print(f"({i})\t{result}\tval")

            if min_metric is None or result < min_metric:
                min_metric = result
                min_model = model
        else:
            min_model = model
            
        optimizer.step()
        optimizer.zero_grad()
    return min_model

def train_mb(model, X_train, y_train, batch_size=64, param_niter=1000, param_delta=1e-02, param_lambda=1e-3):
    N = X_train.shape[0]
    # optimizer = optim.SGD(model.parameters(), lr=param_delta)
    optimizer = optim.Adam(model.parameters(), lr=param_delta)
    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=(1 - 5e-4))
    for i in range(param_niter):
        batch_indices = np.arange(N)
        np.random.shuffle(batch_indices)
        for b in range(0, N, batch_size):
            X_train_batch = X_train[b:b+batch_size]
            y_train_batch = y_train[b:b+batch_size]
            y_pred_batch = model(X_train_batch)
            loss = model.get_loss(y_pred_batch, y_train_batch, param_lambda)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
        if i % 2 == 0:
            L = model.get_loss(model(X_train), y_train, param_lambda)
            print(f"({i})\t{L}")
        scheduler.step()
    return model

def accuracy(Yoh_pred, Yoh_true):
    y_pred = torch.argmax(Yoh_pred, 1).flatten()
    y_true = torch.argmax(Yoh_true, 1).flatten()
    return -torch.mean((y_pred == y_true).float())

dataset_root = '/tmp/mnist'
mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=True)
mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=True)

X_train, y_train = mnist_train.data, mnist_train.targets
X_test, y_test = mnist_test.data, mnist_test.targets
X_train = X_train.float().div_(255.0)
X_test = X_test.float().div_(255.0)

device = 'cuda' if torch.cuda.is_available() else 'cpu'

N = X_train.shape[0]
X_train = X_train.reshape((-1, 28*28)).to(device)
D = X_train.shape[1]
C = y_train.max().add_(1).item()
yoh_train = torch.FloatTensor(data.onehotencode(y_train.numpy())).to(device)
X_test = X_test.reshape((-1, 28*28)).to(device)
yoh_test = torch.FloatTensor(data.onehotencode(y_test.numpy())).to(device)
model = PTDeep([784, 10]).to(device)
print(model.count_params())
LAMBDA = 2e-3
#model = train(model, X_train, yoh_train, 10000, device=device, param_lambda=LAMBDA, param_delta=5e-2, validate=True, metric=accuracy)
# comment out the next line for the random model evaluation
model = train_mb(model, X_train, yoh_train, param_niter=200, param_delta=1e-4, param_lambda=LAMBDA)
probs = model(X_test).detach().cpu().numpy()
preds = np.argmax(probs, axis=1).flatten()
acc = np.sum(preds == y_test.numpy()) / preds.shape[0]
print(f"Accuracy: {acc}")
#torch.save({"model": model, "train_loss": model.get_loss(model(X_train), yoh_train, LAMBDA), "test_loss": model.get_loss(model(X_test), yoh_test, LAMBDA)}, "model_784_100_100_10.pt")
