import data
import numpy as np
import matplotlib.pyplot as plt

class Sigmoid():
    def __init__(self):
        return

    def __call__(self, x):
        return np.reciprocal(1 + np.exp(-x))

    def grad(self, x):
        z = self(x)
        return z * (1 - z)

class ReLU():
    def __init__(self):
        return

    def __call__(self, x):
        x[x<0] = 0
        return x

    def grad(self, x):
        return (x > 0).astype(np.float64)

class Softmax():
    def __init__(self):
        return

    def __call__(self, X):
        X -= np.max(X, axis=1, keepdims=True)
        X = np.exp(X)
        X /= np.sum(X, axis=1, keepdims=True)
        return X


def fcann2_train(X_train, y_train, param_niter=10000, param_delta=2e-2, param_lambda=1e-3, hlsize=8, activation=ReLU(), print_every=100):
    N = X_train.shape[0]
    assert X_train.shape[1] == 2
    assert y_train.shape[0] == N
    y_oh = data.onehotencode(y_train)
    C = y_oh.shape[1]
    W1 = np.random.randn(2, hlsize) * 0.1 #like really small values around 0
    assert W1.shape == (2, hlsize)
    B1 = np.zeros((1, hlsize))
    W2 = np.random.randn(hlsize, C) * 0.1
    B2 = np.zeros((1, C))
    softmax = Softmax()
    for i in range(param_niter):
        s1 = X @ W1 + B1
        assert s1.shape == (N, hlsize)
        h1 = activation(s1)
        assert h1.shape == (N, hlsize)
        s2 = h1 @ W2 + B2
        assert s2.shape == (N, C)
        P_y = softmax(s2)
        assert P_y.shape == (N, C)
        if i % print_every == 0:
            loss = -np.einsum('ij,ij->', y_oh, np.log(P_y)) / N
            print(f"Iteration {i}\t{loss}")
        dL = P_y - y_oh
        assert dL.shape == (N, C)
        dL_dW2 = np.einsum('nc,nh->hc', dL, h1) / N
        assert dL_dW2.shape == W2.shape
        dL_dB2 = (np.einsum('nc->c', dL) / N).reshape(1, -1)
        assert dL_dB2.shape == B2.shape
        dL_ds1 = dL @ W2.T * activation.grad(s1)
        assert dL_ds1.shape == (N, hlsize)
        dL_dW1 = X.T @ dL_ds1
        assert dL_dW1.shape == W1.shape
        dL_dB1 = np.mean(dL_ds1, axis=0, keepdims=True)
        assert dL_dB1.shape == B1.shape

        W2 -= param_delta * dL_dW2
        B2 -= param_delta * dL_dB2
        W1 -= param_delta * dL_dW1
        B1 -= param_delta * dL_dB1
    return W1, B1, W2, B2

def fcann2_classify(X, W1, B1, W2, B2, activation):
    softmax = Softmax()
    return np.argmax(softmax((X @ W1 + B1) @ W2 + B2), axis=1)

if __name__ == "__main__":
    X, y = data.sample_gmm_2d(6, 3, 40)
    activation = ReLU()
    W1, B1, W2, B2 = fcann2_train(X, y, param_niter=100000, print_every=500, activation=activation, param_delta=3e-3)
    y_pred = fcann2_classify(X, W1, B1, W2, B2, activation)
    classifier = lambda X: fcann2_classify(X, W1, B1, W2, B2, activation)
    rect = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(classifier, rect, 0.0, 640, 480)
    data.graph_data(X, y, y_pred)
    plt.show()
