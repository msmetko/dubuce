import data
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

class PTLogreg(nn.Module):
    def __init__(self, D, C):
        super().__init__()
        self.W = nn.Parameter(torch.randn(D, C))
        print(self.W.data)
        self.B = nn.Parameter(torch.zeros(C))
        return

    def forward(self, X):
        X = F.relu(torch.mm(X, self.W) + self.B)
        return F.softmax(X, dim=1)

    def get_loss(self, Y, Yoh, param_lambda=0.01):
        if param_lambda:
            weights = torch.flatten(self.W)
            w = param_lambda *  torch.dot(weights, weights)
        else:
            w = 0
        X = torch.sum(torch.log(Y) * Yoh, 1)
        return -torch.mean(X) + w
        #return F.cross_entropy(Y, Yoh)

def train(model, X, Yoh, param_niter=1000, param_delta=0.5, param_lambda=0.1):
    X = torch.Tensor(X)
    Yoh = torch.Tensor(Yoh)
    optimizer = optim.SGD(model.parameters(), lr=param_delta)
    for i in range(param_niter):
        Y = model(X)
        loss = model.get_loss(Y, Yoh, param_lambda)
        if i % 20 == 0:
            print(f"({i})\t{loss.data.numpy()}")
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
    return model

def evaluate(model, X):
    X = torch.Tensor(X)
    return model(X).detach().numpy()

if __name__ == "__main__":
    np.random.seed(100)
    torch.manual_seed(100)
    X, Y = data.sample_gmm_2d(6, 3, 40)
    Yoh = data.onehotencode(Y)
    print(X.shape, Yoh.shape)
    ptlr = PTLogreg(X.shape[1], Yoh.shape[1])
    ptlr = train(ptlr, X, Yoh, 10000, 0.05, 0.0)
    probs = evaluate(ptlr, X)
    rect = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(lambda X: evaluate(ptlr, X).argmax(axis=1), rect, 0.0, 640, 480)
    data.graph_data(X, Y, np.argmax(probs, axis=1))
    data.plt.show()
