import numpy as np
from sklearn.svm import SVC
import data

class KSVMWrap():
    def __init__(self, X, Y, param_svm_c=1.0, param_svm_gamma='auto'):
        Y = Y.flatten()
        self.svm = SVC(C=param_svm_c, gamma=param_svm_gamma, probability=True)
        self.svm.fit(X, Y)
        return
    
    def predict(self, X):
        return self.svm.predict(X)

    def get_scores(self, X):
        return self.svm.predict_proba(X)

    def support(self):
        return self.svm.support_

if __name__ == "__main__":
    X, Y = data.sample_gmm_2d(4, 2, 30)
    print(X.shape, Y.shape)
    svm = KSVMWrap(X, Y)
    y_pred = svm.predict(X).flatten()
    print(y_pred.shape)
    probs = svm.get_scores(X)[:,0].flatten()
    acc, prec, rec = data.eval_perf_binary(y_pred, Y)
    ap = data.eval_AP(y_pred[np.argsort(probs)[::-1]])
    print(f"Accuracy:\t{acc}\nPrecision:\t{prec}\nRecall:\t\t{rec}\nAP:\t\t{ap}")
    rect = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(lambda x: svm.predict(x), rect)
    data.graph_data(X, Y, y_pred, svm.support())
    data.plt.show()

