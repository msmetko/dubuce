#!/usr/bin/env bash
grep_regex='^\([[:digit:]]+\)'
if [[ $1 == '--val' ]]; then
    grep_regex="${grep_regex}.*val$"
elif [[ $1 == '--train' ]]; then
    grep_regex="${grep_regex}\s[[:digit:].]+$"
fi
grep -E ${grep_regex} | sed -E 's/\(([[:digit:]]+)\)/\1/' | awk '{print $1/2.0, $2}' | gnuplot -p -e 'plot "-"'
