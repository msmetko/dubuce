import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
import data

class PTDeep(nn.Module):
    def __init__(self, layer_list, activation=F.relu):
        super().__init__()
        self.weights = nn.ParameterList()
        self.biases = nn.ParameterList()
        for i, o in zip(layer_list, layer_list[1:]):
            self.weights.append(nn.Parameter(torch.randn(i, o)*0.01))
            self.biases.append(nn.Parameter(torch.zeros(o)))
        self.activation = activation
        return

    def forward(self, X):
        """ X should be a torch.Tensor"""
        for w, b in zip(self.weights, self.biases):
            X = self.activation(torch.mm(X, w) + b)
        return F.softmax(X, dim=1)

    def get_loss(self, Y, Yoh, param_lambda):
        s = 0
        if param_lambda > 0:
            s = param_lambda * sum(torch.sum(w.T @ w) for w in self.weights)
        X = torch.sum(torch.log(Y) * Yoh, 1)
        return -torch.mean(X) + s

    def count_params(self):
        param_sum = 0
        text = "Name\t\tCount\n"
        for name, param in self.named_parameters():
            param_count = np.prod(param.shape)
            text += f"{name}\t{param_count}\n"
            param_sum += param_count
        text += f"\nTotal\t\t{param_sum}"
        return text

def train(model, X, Yoh, param_niter=1000, param_delta=0.1, param_lambda=1.0, device='cpu'):
    #X = torch.Tensor(X).to(device)
    #Yoh = torch.Tensor(Yoh).to(device)
    #print("yoohoo")
    optimizer = optim.SGD(model.parameters(), lr=param_delta)
    #print("henlo optim")
    for i in range(param_niter):
        Y = model(X)
        #print(Y[0, :])
        loss = model.get_loss(Y, Yoh, param_lambda)
        if i % 20 == 0:
            print(f"({i})\t{loss}")
        loss.backward()
        #print(model.weights[1].grad[0, :])
        optimizer.step()
        optimizer.zero_grad()
    return model

def evaluate(model, X):
    X = torch.Tensor(X)
    return model(X).detach().numpy()

if __name__ == "__main__":
    #np.random.seed(100)
    #torch.manual_seed(100)
    X, Y = data.sample_gmm_2d(4, 2, 40)
    #X, Y = data.sample_gmm_2d(6, 2, 10)
    Yoh = data.onehotencode(Y)
    ptdeep = PTDeep([2, 10, 10, 2])
    print(ptdeep.count_params())
    ptdeep = train(ptdeep, X, Yoh, 20000, 0.01, 0.0)
    probs = evaluate(ptdeep, X)
    preds = np.argmax(probs, axis=1)
    acc, prec, rec = data.eval_perf_binary(preds, Y)
    ap = data.eval_AP(preds[probs[:, 0].argsort()[::-1]].flatten())
    print(f"Accuracy:\t{acc}\nPrecision:\t{prec}\nRecall:\t{rec}\nAP:\t\t{ap}")
    rect = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(lambda X: evaluate(ptdeep, X).argmax(axis=1), rect, 0.0, 640, 480)
    data.graph_data(X, Y, np.argmax(probs, axis=1))
    data.plt.show()
