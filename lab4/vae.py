import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torch.optim as optim

from torchvision import datasets, transforms
import tqdm
from torchvision.utils import make_grid

import torch.distributions as tdist

import numpy as np
import tqdm

import seaborn as sns
import pandas as pd

import matplotlib.pyplot as plt

def prepare_data_loaders(batch_size=32):
    train_loader = torch.utils.data.DataLoader(
    torchvision.datasets.MNIST('./MNIST', train=True, download=True,
                               transform=torchvision.transforms.Compose([
                                   torchvision.transforms.ToTensor()
                               ])), batch_size=batch_size)

    test_loader = torch.utils.data.DataLoader(
        torchvision.datasets.MNIST('./MNIST', train=False, download=True,
                                   transform=torchvision.transforms.Compose([
                                       torchvision.transforms.ToTensor()
                                   ])), batch_size=batch_size)
    
    return train_loader, test_loader

def train(model, n_epochs=10, log_epochs=1, batch_size=32, learning_rate=1e-3, device='cpu'):
    train_loader, test_loader = prepare_data_loaders(batch_size)
    
    model = model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    
    model.train()
    
    for epoch_idx in range(0, n_epochs):
        
        train_loss = 0
        for batch_idx, (image_data, _) in enumerate(train_loader):
            image_data = image_data.to(device)
            
            optimizer.zero_grad()
            reconstructed_batch, batch_z, batch_mu, batch_logvar = model(image_data)
            loss = model.loss_fn(reconstructed_batch, image_data, batch_mu, batch_logvar)
            train_loss += loss.item()
            loss.backward()
            optimizer.step()
            
        if epoch_idx % log_epochs == 0:
            print(f"Epoch {epoch_idx+1}/{n_epochs}: {train_loss / (len(train_loader) * train_loader.batch_size):.2f}")
            
    model.eval()
    
    return model

def plot_reconstructions(device='cpu', number_of_samples=10, state_shape=(4, 5)):
    train_loader, test_loader = prepare_data_loaders(batch_size=number_of_samples)
    batch, _ = next(iter(test_loader))
    recons, zs, mus, logvars = model(batch.to(device))
    
    for idx in range(0, number_of_samples):
        original_image = batch[idx, ...].view(28, 28).data.cpu()
        recon_image = recons[idx, ...].view(28, 28).data.cpu()
        state = zs[idx, ...].view(*state_shape).data.cpu()

        plt.figure(figsize=(8, 4))
        plt.subplot(1, 3, 1)
        plt.imshow(original_image)

        plt.subplot(1, 3, 2)
        plt.imshow(recon_image)
        
        plt.subplot(1, 3, 3)
        plt.imshow(state)
        plt.clim(-4, 4)
        plt.colorbar()

def generate_latent_dataframes(data_loader):
    mu_acc = []
    logvar_acc = []
    label_acc = []

    for image_data, label in tqdm.tqdm(data_loader):
        mu, logvar = model.encode(image_data.view(-1, 784).to('cuda'))

        mu_acc.extend(mu.data.cpu().numpy())
        logvar_acc.extend(logvar.data.cpu().numpy())
        label_acc.extend(label.data.cpu().numpy())

    mu_acc = np.array(mu_acc)
    logvar_acc = np.array(logvar_acc)


    tmp = {
        'label': label_acc
    }
    for idx in range(0, mu_acc.shape[1]):
        tmp[f'mu_z{idx}'] = mu_acc[..., idx]

    df_mu = pd.DataFrame(tmp)
    df_mu['label'] = df_mu['label'].astype('category')


    tmp = {
        'label': label_acc
    }
    for idx in range(0, mu_acc.shape[1]):
        tmp[f'logvar_z{idx}'] = np.square(np.exp(logvar_acc[..., idx]))

    df_logvar = pd.DataFrame(tmp)
    df_logvar['label'] = df_logvar['label'].astype('category')


    tmp = {}
    for idx in range(0, model.dec_fc1.weight.T.shape[0]):
        tmp[f'w{idx}'] = list(model.dec_fc1.weight.T[idx, ...].data.cpu().numpy())

    df_dec1_weights = pd.DataFrame(tmp)
    
    return df_mu, df_logvar, df_dec1_weights

def plot_data_boxplots(df_mu, df_logvar, df_dec1_weights, baseline_figsize=(1.2, 6)):
    figwidth, figheight = baseline_figsize
    df_mu2 = df_mu.melt(['label'])
    plt.figure(figsize=(int(figwidth * LATENT_SIZE), figheight))
    sns.boxplot(x='variable', y='value', data=df_mu2)
    plt.title("Distribution of $\mu$ in latent space")

    df_logvar2 = df_logvar.melt(['label'])
    plt.figure(figsize=(int(figwidth * LATENT_SIZE), figheight))
    sns.boxplot(x='variable', y='value', data=df_logvar2)
    plt.title("Distribution of $\sigma^2$ in latent space")

    df_dec1_weights2 = df_dec1_weights.melt()
    plt.figure(figsize=(int(figwidth * LATENT_SIZE), figheight))
    sns.boxplot(x='variable', y='value', data=df_dec1_weights2)
    plt.title("Weights going to decoder from latent space")

def walk_in_latent_space(model, latent_space_abs_limit=3, sqrt_sample_count=20, figsize=(16, 16)):
    canvas = np.zeros((sqrt_sample_count * 28, sqrt_sample_count * 28))

    d1 = np.linspace(-latent_space_abs_limit, latent_space_abs_limit,
                     num=sqrt_sample_count)
    d2 = np.linspace(-latent_space_abs_limit, latent_space_abs_limit,
                     num=sqrt_sample_count)

    D1, D2 = np.meshgrid(d1, d2)
    synth_reps = np.array([D1.flatten(), D2.flatten()]).T
    synth_reps_pt = torch.from_numpy(synth_reps).float().to("cuda:0")

    recons = model.decode(synth_reps_pt)

    for idx in range(0, sqrt_sample_count * sqrt_sample_count):
        x, y = np.unravel_index(idx, (sqrt_sample_count, sqrt_sample_count))

        sample_offset = sqrt_sample_count - x - 1

        first_from = 28 * sample_offset
        first_to = 28 * (sample_offset + 1)

        second_from = 28 * y
        second_to = 28 * (y + 1)

        canvas[first_from: first_to, second_from:second_to] = recons[idx, ...].view(28, 28).data.cpu().numpy()
    plt.figure(figsize=figsize)
    plt.imshow(canvas)
    return

class VAE(nn.Module):
    def __init__(self, latent_size, hidden_size):
        super(VAE, self).__init__()
        
        self.latent_size = latent_size
        self.enc_fc1 = nn.Linear(784, hidden_size)
        self.enc_fc2 = nn.Linear(hidden_size, hidden_size)
        self.mu = nn.Linear(hidden_size, latent_size)
        self.logvar = nn.Linear(hidden_size, latent_size)
        self.dec_fc1 = nn.Linear(latent_size, hidden_size)
        self.dec_fc2 = nn.Linear(hidden_size, hidden_size)
        self.output = nn.Linear(hidden_size, 784)
        self.softplus = nn.Softplus()
        return
        
    def encode(self, x):
        x = self.enc_fc1(x)
        x = self.softplus(x)
        x = self.enc_fc2(x)
        x = self.softplus(x)
        mu = self.mu(x)
        logvar = self.logvar(x)
        return mu, logvar
        
    def reparametrize(self, mu, logvar):
        noise = torch.randn_like(mu)
        return mu + torch.sqrt(torch.exp(logvar)) * noise
    
    def decode(self, z):
        z = self.dec_fc1(z)
        z = self.softplus(z)
        z = self.dec_fc2(z)
        z = self.softplus(z)
        z = self.output(z)
        z = torch.sigmoid(z)
        return z
    
    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, 784))
        z = self.reparametrize(mu, logvar)
        reconstruction = self.decode(z)
        
        return reconstruction, z, mu, logvar
    
    @staticmethod
    def loss_fn(reconstruction, batch, mu, logvar):
        batch = batch.view(-1, 784)
        # print(reconstruction.size(), batch.size())
        var = torch.exp(logvar)
        crossentropy = -torch.sum(batch * torch.log(reconstruction) + (1-batch)*torch.log(1-reconstruction), 1) 
        kl_div = torch.sum(mu**2 + var - logvar - 1, 1)/2
        return torch.mean(kl_div + crossentropy)


LATENT_SIZE = 2
HIDDEN_SIZE = 200
EPOCHS = 100
model = VAE(LATENT_SIZE, HIDDEN_SIZE).cuda()
model = train(model, batch_size=1024, device='cuda:0', n_epochs=EPOCHS , log_epochs=10, learning_rate=1.5e-4)

plot_reconstructions('cuda:0', state_shape=(2, 1))

_, test_loader = prepare_data_loaders()
df_mu, df_logvar, df_dec1_weights = generate_latent_dataframes(test_loader)
plt.figure(figsize=(16, 16))
sns.scatterplot(x='mu_z0', y='mu_z1', hue='label', s=50, data=df_mu)

plot_data_boxplots(df_mu, df_logvar, df_dec1_weights)

walk_in_latent_space(model, latent_space_abs_limit=1.5, sqrt_sample_count=15)
plt.show()
