import numpy as np
import data
import matplotlib.pyplot as plt

def sigm(x):
    return 1 / (1 + np.exp(-x))

def ce_loss(*, y_true=None, y_pred=None):
    N = y_true.shape[0]
    return -(1/N) * np.sum(y_true * np.log(y_pred) + (1 - y_true)*np.log(1-y_pred))

def binlogreg_train(X_train, y_train, param_niter=10000, param_delta=1e-2):
    N, D = X_train.shape
    assert D == 2
    assert y_train.shape == (N, 1)
    w = np.random.randn(D, 1)*0.01
    b = np.array([[0.0]])
    for i in range(param_niter):
        scores = X_train @ w + b
        assert scores.shape == (N, 1)
        probs = sigm(scores)
        assert probs.shape == (N, 1)
        loss = ce_loss(y_true=y_train, y_pred=probs)
        assert isinstance(loss, float)
        #if i % 10 == 0:
        #    print("Iteration {}\tLoss: {}".format(i, loss))
        dL_scores = probs - y_train
        assert dL_scores.shape == (N, 1)
        grad_w = (1/N) * X_train.T @ dL_scores
        assert grad_w.shape == (D, 1)
        grad_b = np.sum(grad_w, keepdims=True)
        w -= param_delta * grad_w
        b -= param_delta * grad_b
    return w, b

def binlogreg_classify(X, w, b):
    return sigm(X @ w + b)

def binlogreg_decfun(w, b):
    return lambda X: binlogreg_classify(X, w, b)

if __name__ == "__main__":
    X_train, y_train = data.sample_gauss_2d(2, 100)
    w, b = binlogreg_train(X_train, y_train)
    print(w, b)
    y_prob = binlogreg_classify(X_train, w, b)
    y_pred = y_prob >= 0.5
    acc, rec, prec = data.eval_perf_binary(y_pred, y_train)
    AP = data.eval_AP(y_train[y_prob.argsort(axis=0)])
    print("Accuracy \t {}\nRecall \t {}\nPrecision \t {}\nAP \t {}".format(acc, rec, prec, AP))
    decfun = binlogreg_decfun(w, b)
    bbox = (np.min(X_train, axis=0), np.max(X_train, axis=0))
    data.graph_surface(decfun, bbox, 0.5, 640, 480)
    data.graph_data(X_train, y_train, y_pred)
    plt.show()
