import numpy as np
import data
import matplotlib.pyplot as plt

def softmax(x):
    x -= x.max(axis=1, keepdims=True)
    x = np.exp(x)
    return x / x.sum(axis=1, keepdims=True)

def ce_loss(*, y_true=None, y_pred=None):
    assert y_true.shape == y_pred.shape
    N = y_true.shape[0]
    return -np.sum(y_true * np.log(y_pred)) / N

def logreg_train(X_train, y_train, param_niter=10000, param_delta=1e-2):
    N, D = X_train.shape
    C = np.max(y_train) + 1
    assert D == 2
    assert y_train.shape == (N, 1)
    w = np.random.randn(D, C)
    b = np.zeros((1, C))
    # stack overflow ONE HOT ENCODING
    # https://stackoverflow.com/questions/29831489
    y_train = y_train.flatten()
    y_ohc = np.zeros((y_train.size, C))
    y_ohc[np.arange(y_train.size), y_train] = 1
    for i in range(param_niter):
        scores = X_train @ w + b
        assert scores.shape == (N, C)
        probs = softmax(scores)
        assert probs.shape == (N, C)
        loss = ce_loss(y_true=y_ohc, y_pred=probs)
        assert isinstance(loss, float)
        #if i % 10 == 0:
        #    print("Iteration {}\tLoss: {}".format(i, loss))
        dL_scores = probs - y_ohc
        assert dL_scores.shape == (N, C)
        grad_w = (1/N) * X_train.T @ dL_scores
        assert grad_w.shape == (D, C)
        assert grad_w.shape == w.shape
        grad_b = np.sum(grad_w, axis=0, keepdims=True)
        assert grad_b.shape == b.shape
        w -= param_delta * grad_w
        b -= param_delta * grad_b
    return w, b

def logreg_max(w, b):
    return lambda x: np.argmax(softmax(x @ w + b), axis=1)

if __name__ == "__main__":
    X_train, y_train = data.sample_gauss_2d(4, 100)
    w, b = logreg_train(X_train, y_train, param_niter=10000)
    print(w, b)
    y_prob = softmax(X_train @ w + b)
    #y_pred = y_prob >= 0.5
    #acc, rec, prec = data.eval_perf_binary(y_pred, y_train)
    #AP = data.eval_AP(y_train[y_prob.argsort(axis=0)])
    #print("Accuracy \t {}\nRecall \t {}\nPrecision \t {}\nAP \t {}".format(acc, rec, prec, AP))
    y_pred = np.argmax(y_prob, axis=1)
    decfun = logreg_max(w, b)
    bbox = (np.min(X_train, axis=0), np.max(X_train, axis=0))
    #y_pred = y_prob >= 0.5
    data.graph_surface(decfun, bbox, 0.5, 640, 480)
    data.graph_data(X_train, y_train, y_pred)
    plt.show()

