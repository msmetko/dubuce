#!/usr/bin/env python3
import numpy as np
grid = dict()
def stats(value_dict, name):
    total_length = sum(len(v) for k, v in value_dict.items())
    values = np.zeros((total_length, 2))
    i = 0
    for key in value_dict:
        for value in value_dict[key]:
            values[i, :] = (key, value)
            i += 1
    print(f"Correlation of {name}: {np.corrcoef(values, rowvar=False)[0, 1]}")

try:
    while True:
        _, h, n, d, b = input().strip().split(' ')
        h = int(h)
        n = int(n)
        d = float(d)
        b = False if b == "False" else True
        input()
        input()
        input()
        _, acc, _ = input().strip().split('\t')
        grid[(h, n, d, b)] = float(acc)
        # print(f"{h} {n} {d} {b}")
        input()
        input()
except:
    h_dict = {50: [], 150: [], 250: []}
    n_dict = {2: [], 3: [], 4: []}
    d_dict = {0.25: [], 0.5: [], 0.75: []}
    b_dict = {True: [], False: []}
    for key, value in grid.items():
        h, n, d, b = key
        h_dict[h].append(value)
        n_dict[n].append(value)
        d_dict[d].append(value)
        b_dict[b].append(value)
    stats(h_dict, "h")
    stats(n_dict, "n")
    stats(d_dict, "d")
    stats(b_dict, "b")
    best_arch = max(grid, key=grid.get)
    print(f"Best arch: {best_arch} with acc of {grid[best_arch]}")
