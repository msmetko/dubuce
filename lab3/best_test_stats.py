#!/usr/bin/env python3
import numpy as np
accuracies = []
f1_1s = []
f1_2s = []
T_DIST_9_DEG_95 = 2.262

def conf(values):
    t = T_DIST_9_DEG_95
    m = np.mean(values)
    s = np.std(values, ddof=1)
    return (m-t*s, m+t*s)

try:
    while True:
        input()
        input()
        input()
        _, acc, _ = input().strip().split('\t')
        accuracies.append(float(acc))
        _, f1_1, f1_2 = input().strip().split('\t')
        f1_1s.append(float(f1_1))
        f1_2s.append(float(f1_2))
        input()
except:
    print(f"Acc\t{conf(accuracies)}")
    print(f"f1 1\t{conf(f1_1s)}")
    print(f"f1 2\t{conf(f1_2s)}")
