import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from loading import bootstrap, my_collate as collate
from sklearn.metrics import confusion_matrix

class Model(nn.Module):
    def __init__(self, embeddings, hidden_size=150):
        super().__init__()
        self.embeddings = embeddings
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(input_size=300, hidden_size=hidden_size, num_layers=2, batch_first=True)
        self.fc1 = nn.Linear(150, 150)
        self.fc2 = nn.Linear(150, 1)
        return

    def forward(self, x):
        # print(type(x))
        x = self.embeddings(x)
        #x = nn.utils.rnn.pack_sequence([*x], enforce_sorted=False)
        B, T, E = x.size()
        _, (h, _) = self.lstm(x, None)
        # print(h.size())
        h = h.view(2, 1, B, self.hidden_size)
        #raise Exception
        x = h[1, 0, :, :]
        x = self.fc1(x)
        x = torch.relu(x)
        x = self.fc2(x)
        # x = torch.relu(x)
        #print(x.size())
        return x

def accuracy(cm):
    # return cm[0, 0] / np.sum(np.diag(cm))
    return np.sum(np.diag(cm)) / np.sum(cm)

def recall(cm):
    return cm[0, 0] / np.sum(cm[:, 0])

def precision(cm):
    return cm[0, 0] / np.sum(cm[0, :])

def f1score(cm):
    p = precision(cm)
    r = recall(cm)
    return 2*(p * r) / (p + r)

def get_class_cm(cm, index):
    """
    This function takes a confusion matrix from sklearn
    and returns a perclass binary confusion matrix
    
    Note that this function changes conventions!
    - sklearn's CM has true labels at rows and predicted at columns
        [[tp, fn],
         [fp, tn]]
    - this CM has true labels at columns and predicted at rows
    this means this CM is arranged as:
        [[tp, fp],
         [fn, tn]]
    """
    tp = cm[index, index]
    fn = np.sum(cm[index, :]) - tp
    fp = np.sum(cm[:, index]) - tp
    tn = np.sum(cm) - tp - fp - fn
    return np.array([[tp, fp],[fn, tn]])

def metrics(y_pred, y_true):
    cm = confusion_matrix(y_true, y_pred)
    print(cm.T)
    print("\t1\t2")
    for m in ["acc", "f1"]:
        print(m, end='\t')
        for i in range(len(cm)):
            cm_i = get_class_cm(cm, i)
            if m == "acc":
                print(f"{accuracy(cm_i):.04}", end='\t')
            else:
                print(f"{f1score(cm_i):.04}", end='\t')
        print()
    print()
    return
 
def train(model, dataloader, opt, criterion, loss_callback):
    model.train()
    for i, (inputs, labels, lengths) in enumerate(dataloader):
        opt.zero_grad()
        outputs = model(inputs)
        labels = labels.type_as(outputs)
        loss = criterion(outputs.squeeze(), labels)
        #loss = criterion(outputs, labels)
        #loss_callback(i, loss.item())
        loss.backward()
        nn.utils.clip_grad_norm_(model.parameters(), 0.25)
        opt.step()
    return

def evaluate(model, dl_val, criterion):
    model.eval()
    with torch.no_grad():
        full_labels = []
        full_logits = []
        for i, (inputs, labels, lengths) in enumerate(dl_val):
            full_labels += labels.cpu().numpy().tolist()
            logits = model(inputs)
            full_logits += logits.squeeze().cpu().numpy().tolist()
        full_labels = np.array(full_labels)
        full_logits = np.array(full_logits)
        full_logits = (full_logits > 0).astype(int)
        metrics(full_logits, full_labels)
    return

def main():
    #np.random.seed(7052020)
    #torch.manual_seed(7052020)
    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    ds_train, ds_test, ds_valid, embeddings = bootstrap(device=device)
    dl_train = DataLoader(ds_train, batch_size=10, collate_fn=collate, shuffle=True)
    dl_valid = DataLoader(ds_valid, batch_size=len(ds_valid), collate_fn=collate)
    dl_test = DataLoader(ds_test, batch_size=len(ds_test), collate_fn=collate)
    model = Model(embeddings).to(device)
    opt = optim.Adam(model.parameters(), lr=1e-4)
    loss = nn.BCEWithLogitsLoss()
    for epoch in range(5):
        train(model, dl_train, opt, loss, lambda *t: print(epoch, *t))
        evaluate(model, dl_valid, loss)
    print("Test")
    evaluate(model, dl_test, loss)

if __name__ == "__main__":
    main()
