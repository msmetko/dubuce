from dataclasses import dataclass
from collections import Counter

import torch
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn

FIELD_SEPARATOR = ", "

@dataclass
class Instance:
    text: str
    label: str

class Vocab():
    def __init__(self, string_list, max_size=-1, min_freq=0, word_sep=" ", special=False):
        assert max_size >= -1
        #if max_size == -1:
        #    max_size = len(string_list)

        self.stoi = dict()
        self.itos = dict()
        self.counter = Counter()
        for sentence in string_list:
            #print(sentence.split(word_sep))
            self.counter.update(sentence.split(word_sep))

        # python is _very_ elegant language
        sorted_iter = iter(self.counter.most_common(None if max_size == -1 else max_size))
        sorted_words = list(word for word, count in sorted_iter if count >= min_freq)
        if special:
            sorted_words = ["<PAD>", "<UNK>"] + sorted_words
        for i, word in enumerate(sorted_words):
            self.stoi[word] = i
            self.itos[i] = word
        return

    def encode(self, word):
        return self.stoi.get(word, self.stoi.get("<UNK>", None))

    def decode(self, index):
        self.itos.get(index, self.itos[0])

    def __len__(self):
        return len(self.stoi)


class NLPDataset(Dataset):
    def __init__(self, instance_list, text_vocab, label_vocab, device="cpu"):
        self.instances = instance_list
        self.text_vocab = text_vocab
        self.label_vocab = label_vocab
        self.device = device
        return

    def __getitem__(self, index):
        input_instance = self.instances[index]
        return (
            torch.tensor([self.text_vocab.encode(t) for t in input_instance.text.split(' ')]).to(self.device),
            torch.tensor(self.label_vocab.encode(input_instance.label)).to(self.device))

    def __len__(self):
        return len(self.instances)

def preprocess(dataset_row, sep):
    return [
        Instance(*values)
        for values in map(lambda t: t.strip().split(sep), dataset_row)]

def my_collate(batch, pad_index=0):
    texts, labels = zip(*batch)
    lengths = torch.tensor(list(map(len, texts)))
    texts = nn.utils.rnn.pad_sequence(texts, batch_first=True, padding_value=pad_index)
    return texts, torch.tensor(labels), lengths

def embedding_matrix(filename, vocab, dimensionality=300, freeze=True):
    embedding = torch.randn(len(vocab), dimensionality)
    if filename:
        with open(filename, 'r') as file:
            for line in file:
                line = line.strip().split(' ')
                word = line[0]
                word_index = vocab.encode(word)
                assert word_index != 1
                embedding[word_index, :] = torch.tensor(list(map(float, line[1:])))
    embedding[0, :] = 0.0
    return nn.Embedding.from_pretrained(embedding, freeze=(freeze or filename is not None), padding_idx=0)

def bootstrap(device="cpu", freeze=True):
    with open("sst_train_raw.csv") as file:
        instances = preprocess(file.readlines(), FIELD_SEPARATOR)
        text_vocab = Vocab([instance.text for instance in instances], special=True)
        label_vocab = Vocab([instance.label for instance in instances], special=False)
        ds_train = NLPDataset(instances, text_vocab, label_vocab, device)

    with open("sst_valid_raw.csv") as file:
        instances = preprocess(file.readlines(), FIELD_SEPARATOR)
        ds_valid = NLPDataset(instances, text_vocab, label_vocab, device)

    with open("sst_test_raw.csv") as file:
        instances = preprocess(file.readlines(), FIELD_SEPARATOR)
        ds_test = NLPDataset(instances, text_vocab, label_vocab, device)

    em_m = embedding_matrix('sst_glove_6b_300d.txt', text_vocab)
    return ds_train, ds_valid, ds_test, em_m.to(device)

if __name__ == "__main__":
    with open("sst_train_raw.csv") as file:
        instances = preprocess(file.readlines(), FIELD_SEPARATOR)
        text_vocab = Vocab([instance.text for instance in instances], special=True)
        label_vocab = Vocab([instance.label for instance in instances], special=False)
        assert text_vocab.counter["the"] == 5954
        assert text_vocab.counter["a"] == 4361
        assert text_vocab.counter["and"] == 3831
        assert text_vocab.counter["of"] == 3631
        assert text_vocab.counter["to"] == 2438
        assert text_vocab.encode("<PAD>") == 0
        assert text_vocab.encode("<UNK>") == 1
        assert text_vocab.encode("the") == 2
        assert text_vocab.encode("a") == 3
        assert text_vocab.encode("and") == 4, text_vocab.encode("the")
        assert text_vocab.encode("my") == 188
        assert text_vocab.encode("twists") == 930
        assert text_vocab.encode("lets") == 956
        assert text_vocab.encode("sports") == 1275
        assert text_vocab.encode("amateurishly") == 6818
        assert len(text_vocab.itos) == 14806

        # this makes no sense really, but reality is often dissapointing
        assert label_vocab.encode("positive") == 0
        assert label_vocab.encode("negative") == 1
        #print(embedding_matrix('sst_glove_6b_300d.txt', text_vocab))
        ds_train = NLPDataset(instances, text_vocab, label_vocab)
        assert torch.equal(ds_train[3][0], torch.tensor([189, 2, 674, 7, 129, 348, 143]))
        assert torch.tensor(0) == ds_train[3][1]
        train_dl = DataLoader(ds_train, batch_size=2, shuffle=False, collate_fn=my_collate)
        texts, labels, lengths = next(iter(train_dl))
        assert torch.equal(
            texts,
            torch.tensor([[2, 554, 7, 2872, 6, 22, 2, 2873, 1236,    8,   96, 4800,
                       4,   10,   72,    8,  242,    6,   75,    3, 3576,   56, 3577,   34,
                    2022, 2874, 7123, 3578, 7124,   42,  779, 7125,    0,    0],
                    [   2, 2875, 2023, 4801,    5,    2, 3579,    5,    2, 2876, 4802,    7,
                      40,  829,   10,    3, 4803,    5,  627,   62,   27, 2877, 2024, 4804,
                     962,  715,    8, 7126,  555,    5, 7127, 4805,    8, 7128]]))
        assert torch.equal(labels, torch.tensor([0, 0]))
        assert torch.equal(lengths, torch.tensor([32, 34]))
    #with open('sst_test_raw.csv') as file:
    #    instances = preprocess(file.readlines(), FIELD_SEPARATOR)
    #    ds_test = NLPDataset(instances, text_vocab, label_vocab)
    #with open('sst_valid_raw.csv') as file:
    #    instances = preprocess(file.readlines(), FIELD_SEPARATOR) 
    #    ds_valid = NLPDataset(instances, text_vocab, label_vocab)


